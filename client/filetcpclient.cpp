#include "filetcpclient.h"
#include <QMessageBox>
#include <QFile>

FileTcpClient::FileTcpClient(QObject *parent) : QObject(parent), file(NULL)
{
    tcpSocket = new QTcpSocket(this);
    connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(displayError(QAbstractSocket::SocketError)));
}

void FileTcpClient::readyRead()
{
    QDataStream in(tcpSocket);
    in.setVersion(QDataStream::Qt_4_0);

    if (blockSize == 0) {
        if (tcpSocket->bytesAvailable() < (int)sizeof(quint16))
            return;
        in >> blockSize;
    }

    if (tcpSocket->bytesAvailable() < blockSize)
        return;

    //qDebug() << "reseive: ";


    QByteArray str;
    in >> str;
    blockCnt += str.size();
    qDebug() << "reseive: " << str.size() << "filesize: " << file->size() << "/" << blockCnt;
    emit progress(blockCnt, file->size());
    blockSize = 0;

    sendFileBlock();
}

void FileTcpClient::displayError(QAbstractSocket::SocketError socketError)
{
    QString strError;
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:

        strError = ("The host was not found. Please check the host name and port settings.");

        break;
    case QAbstractSocket::ConnectionRefusedError:
        strError = ("The connection was refused by the peer. "
                                    "Make sure the fortune server is running, "
                                    "and check that the host name and port "
                                    "settings are correct.");
        break;
    default:
        strError = (tr("The following error occurred: %1.")
                                 .arg(tcpSocket->errorString()));
    }
    if(strError.isEmpty())
    {
        strError = tcpSocket->errorString();
    }
    emit error(strError);
    emit message(strError);
    qDebug() << tcpSocket->errorString();
}

void FileTcpClient::connectToServer(QString ip, int port)
{
    emit message("connected");
    blockSize = 0;
    tcpSocket->abort();
    tcpSocket->connectToHost(ip, port);

}

void FileTcpClient::sendFile(QString filePath)
{
    if(file != NULL)
    {
        file->close();
        delete file;
    }
    file = new QFile(filePath);
    bool res = file->open(QFile::ReadOnly);
    if(!res)
    {
        emit error("File is not open");
        qDebug() << "file is not open";
        return;
    }
    qDebug() << "send file";
    blockCnt = 0;
    sendFileBlock();

}

void FileTcpClient::sendFileBlock()
{
    QByteArray block;
    QByteArray data = file->read(300);
    if(data.size() == 0)
    {
        qDebug() << "end";
        return;
    }

    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);

    out << (quint16)0;
    out << data;
    out.device()->seek(0);
    out << (quint16)(block.size() - sizeof(quint16));

    tcpSocket->write(block);
}

void FileTcpClient::disconnectFromServer()
{
    tcpSocket->disconnectFromHost();
}

void FileTcpClient::test()
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);

    out << (quint16)0;
    out << QString("test message");
    out.device()->seek(0);
    out << (quint16)(block.size() - sizeof(quint16));

    tcpSocket->write(block);
}



