#ifndef FILETCPCLIENT_H
#define FILETCPCLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QFile>

class FileTcpClient : public QObject
{
    Q_OBJECT
public:
    explicit FileTcpClient(QObject *parent = 0);
    void connectToServer(QString ip, int port);
    void sendFile(QString filePath);
private:
    void sendFileBlock();
private:
    QTcpSocket *tcpSocket;
    quint16 blockSize = 0;
    int blockCnt;

private:
    QFile *file;

signals:
    void progress(int value, int maxValue);
    void error(QString strError);
    void message(QString msg);
public slots:
private slots:
    void readyRead();
    void displayError(QAbstractSocket::SocketError socketError);
    void disconnectFromServer();
    void test();

};

#endif // FILETCPCLIENT_H
