#-------------------------------------------------
#
# Project created by QtCreator 2016-06-04T10:16:58
#
#-------------------------------------------------

QT       += core gui
QT += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FileUploader
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    client/filetcpclient.cpp \
    model/clientmodel.cpp \
    view/clientview.cpp

HEADERS  += mainwindow.h \
    client/filetcpclient.h \
    model/clientmodel.h \
    view/clientview.h
