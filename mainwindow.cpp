#include "mainwindow.h"
#include <QMessageBox>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    ftcpClient = new FileTcpClient;
    cm = new ClientModel;
    cv = new ClientView(cm);
    connect(cv, SIGNAL(connectToServer()), SLOT(connectToServer()));
    connect(cv, SIGNAL(disconnectFormServer()), ftcpClient, SLOT(disconnectFromServer()));
    connect(cv, SIGNAL(test()), ftcpClient, SLOT(test()));
    connect(cv, SIGNAL(send()), SLOT(sendFile()));

    connect(ftcpClient, SIGNAL(progress(int,int)), cv, SLOT(setProgressBar(int,int)));

    connect(ftcpClient, SIGNAL(error(QString)), SLOT(displayError(QString)));
    connect(ftcpClient, SIGNAL(message(QString)), cv, SLOT(labelConnect(QString)));

    setCentralWidget(cv);
}

MainWindow::~MainWindow()
{
    delete cv;
}

void MainWindow::connectToServer()
{
    ftcpClient->connectToServer(cm->getAddress(), cm->getPort());
}

void MainWindow::sendFile()
{
    ftcpClient->sendFile(cm->getFileName());
}

void MainWindow::displayError(QString error)
{
    QMessageBox::information(this, "", error);
}
