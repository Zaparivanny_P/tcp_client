#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "view/clientview.h"
#include "model/clientmodel.h"
#include "client/filetcpclient.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
private:
    ClientView *cv;
    ClientModel *cm;
    FileTcpClient *ftcpClient;
private slots:
    void connectToServer();
    void sendFile();
    void displayError(QString error);
};

#endif // MAINWINDOW_H
