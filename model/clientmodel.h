#ifndef CLIENTMODEL_H
#define CLIENTMODEL_H

#include <QString>
#include <QFile>

class ClientModel
{
public:
    ClientModel();
    int getPort();
    QString getAddress();
    QString getFileName()
    {
        return filename;
    }

    void setFileName(QString filename)
    {
        this->filename = filename;
    }

private:
    QString filename;
};

#endif // CLIENTMODEL_H
