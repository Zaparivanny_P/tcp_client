#ifndef CLIENTVIEW_H
#define CLIENTVIEW_H

#include <QWidget>
#include <QLayout>
#include <QPushButton>
#include <QProgressBar>
#include <QLabel>
#include "model/clientmodel.h"

class ClientView : public QWidget
{
    Q_OBJECT
public:
    explicit ClientView(ClientModel *cm, QWidget *parent = 0);
    ~ClientView();
private:
    QLayout *vlay;
    QPushButton *btnConnect;
    QPushButton *btnDisconnect;
    QPushButton *btnOpenFile;
    QPushButton *btnSend;
    QPushButton *btnTest;

    QProgressBar *proggress;
    QLabel *connectLabel;

    ClientModel *cm;
signals:
    void connectToServer();
    void disconnectFormServer();
    void test();
    void send();


public slots:
    void fileSelect();
    void setProgressBar(int value, int maxValue);
    void labelConnect(QString text);
};

#endif // CLIENTVIEW_H
