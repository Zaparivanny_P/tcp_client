#include "clientview.h"
#include <QFileDialog>

ClientView::ClientView(ClientModel *cm, QWidget *parent) : QWidget(parent), cm(cm)
{
    vlay = new QVBoxLayout;
    btnConnect = new QPushButton("Connect");
    btnDisconnect = new QPushButton("Disconnect");

    btnOpenFile = new QPushButton("Open file");
    btnSend = new QPushButton("Send file");
    btnTest = new QPushButton("Test");

    proggress = new QProgressBar();
    connectLabel = new QLabel;

    connect(btnConnect, SIGNAL(released()), SIGNAL(connectToServer()));
    connect(btnDisconnect, SIGNAL(released()), SIGNAL(disconnectFormServer()));
    connect(btnTest, SIGNAL(released()), SIGNAL(test()));
    connect(btnOpenFile, SIGNAL(released()), SLOT(fileSelect()));
    connect(btnSend, SIGNAL(released()), SIGNAL(send()));

    vlay->addWidget(btnConnect);
    vlay->addWidget(connectLabel);
    vlay->addWidget(btnDisconnect);
    vlay->addWidget(btnOpenFile);
    vlay->addWidget(btnSend);
    vlay->addWidget(btnTest);
    vlay->addWidget(proggress);

    vlay->setAlignment(Qt::AlignLeft);
    setLayout(vlay);
}

ClientView::~ClientView()
{
    delete btnDisconnect;
    delete btnConnect;
    delete vlay;
}

void ClientView::fileSelect()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open file"), "d:/", tr("Files (*.*)"));
    if(!fileName.isEmpty())
    {
        cm->setFileName(fileName);
    }
}

void ClientView::setProgressBar(int value, int maxValue)
{
    proggress->setMaximum(maxValue);
    proggress->setValue(value);
}

void ClientView::labelConnect(QString text)
{
    connectLabel->setText(text);
}
